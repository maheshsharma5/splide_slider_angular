import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  splideOptions = { autoWidth: true, pagination: false, arrows: false, gap: 32 };
}
